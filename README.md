# watermark

#### 介绍
java实现pdf,word,excel,ppt,图片加水印

基于spire.office.free-5.3.1.jar实现的.

由于是免费版,会有一些官方的水印,ppt,pdf加水印限制为10页

excel的水印在打印的时候再能看到,或者点击页面布局打印预览也可看到

图片加水印不需要spire.office.free-5.3.1.jar支持

gitee上的项目不是maven,可能需要手动导下包

博客地址:https://blog.csdn.net/qq_41635282/article/details/126378054
上面有测试图