package com.bingjiu.utils;

import com.spire.doc.*;
import com.spire.doc.FileFormat;
import com.spire.doc.documents.WatermarkLayout;

import java.awt.*;
import java.awt.geom.Dimension2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import com.spire.pdf.PdfDocument;
import com.spire.pdf.PdfPageBase;
import com.spire.pdf.graphics.*;
import com.spire.presentation.IAutoShape;
import com.spire.presentation.PortionEx;
import com.spire.presentation.Presentation;
import com.spire.presentation.ShapeType;
import com.spire.presentation.drawing.FillFormatType;
import com.spire.xls.ExcelVersion;
import com.spire.xls.ViewMode;
import com.spire.xls.Workbook;
import com.spire.xls.Worksheet;


import static java.awt.image.BufferedImage.TYPE_INT_ARGB;

/**
 * doc,docx,pdf,ppt 附件加水印
 * 免费版有水印且pdf,ppt只能处理10页
 * @author : Xiewy <br/>
 * @date : 2022/8/16 15:54 <br/>
 */
public class SpireWaterMarkUtils {
    public static void main(String[] args) throws Exception {
        //word加水印
//        setWordWaterMark("d:/tmp/tmp/2.doc", "d:/tmp/tmp/3.doc", "槟榔与酒的醉人故事");
//        setWordWaterMark("d:/tmp/tmp/2.docx", "d:/tmp/tmp/3.docx", "槟榔与酒的醉人故事");
        //xls加水印
//        setExcelWaterMark("C:\\Users\\BetelNutsAndWine\\Desktop\\table_export.xlsx", "C:\\Users\\BetelNutsAndWine\\Desktop\\4444.xlsx", "槟榔与酒的醉人故事");
//        setExcelWaterMark("d:/tmp/tmp/2.xlsx", "d:/tmp/tmp/3.xlsx", "槟榔与酒的醉人故事");
        //ppt加水印
        setPPTWaterMark("C:\\Users\\BetelNutsAndWine\\Desktop\\与爱同行.pptx", "C:\\Users\\BetelNutsAndWine\\Desktop\\ppt222.pptx", "槟榔与酒的醉人故事");
//        setPPTWaterMark("d:/tmp/tmp/2.pptx", "d:/tmp/tmp/3.pptx", "槟榔与酒的醉人故事");
//        setPPTWaterMark("d:/tmp/tmp/22.ppt", "d:/tmp/tmp/33.ppt", "槟榔与酒的醉人故事");
        //pdf
//        setPdfWaterMark("C:\\Users\\BetelNutsAndWine\\Desktop\\d99d7d30-17d3-47c6-b67e-56b93b60f346.pdf","C:\\Users\\BetelNutsAndWine\\Desktop\\pdf111.pdf","槟榔与酒的醉人故事 2022.08.16 00:00:00 122.222.222");
    }

    public static final String FOND_ST = "宋体";

    //--word 加水印start-----------------------------------------------------------------------------------------
    /**
     * Word添加文本水印
     * @param originalFilePath 待加水印word的路径 例如:C:\Desktop\textWatermark.docx
     * @param newFilePath 水印word的文件存储路径  例如:C:\Desktop\watermark2.docx
     * @param txtWatermark 文本水印类
     */
    public static Boolean wordWatermark(String originalFilePath, String newFilePath, TextWatermark txtWatermark){
        boolean temp;
        if (txtWatermark == null){
            SpireWaterMarkUtils.getTextWatermark("水印");
        }
        try {
            //加载文档
            Document document = new Document();
            document.loadFromFile(originalFilePath);
            //插入文本水印
            Section section = document.getSections().get(0);
            section.getDocument().setWatermark(txtWatermark);
            //保存水印文档
            document.saveToFile(newFilePath, FileFormat.Docx);
            temp  = true;
        } catch (Exception e) {
            temp  = false;
            e.printStackTrace();
        }
        return temp;
    }

    /**
     * 获取文本水印默认类-word
     * @param watermarkText 水印内容
     * @return TextWatermark
     */
    public static TextWatermark getTextWatermark(String watermarkText){
        TextWatermark txtWatermark = new TextWatermark();
        txtWatermark.setText(watermarkText);
        txtWatermark.setFontSize(25);
        txtWatermark.setColor(Color.red);
        txtWatermark.setLayout(WatermarkLayout.Diagonal);
        return txtWatermark;
    }

    /**
     * 自定义文本水印类-word
     * @param watermarkText 水印内容
     * @param fontSize 字体大小
     * @param color 字体颜色
     * @return TextWatermark
     */
    public static TextWatermark getTextWatermark(String watermarkText,int fontSize,Color color){
        TextWatermark txtWatermark = new TextWatermark();
        txtWatermark.setText(watermarkText);
        txtWatermark.setFontSize(fontSize);
        txtWatermark.setColor(color);
        txtWatermark.setLayout(WatermarkLayout.Diagonal);
        return txtWatermark;
    }

    //--word 加水印end-----------------------------------------------------------------------------------------


    //--excel 加水印start-----------------------------------------------------------------------------------------
    /**
     * excel设置水印
     * Excel 水印在正常模式下不可见，仅在页面布局模式或打印预览模式可见。
     *
     * @param inputPath
     * @param outPath
     * @param markStr
     */
    public static void setExcelWaterMark(String inputPath, String outPath, String markStr) {
        //加载示例文档
        Workbook workbook = new Workbook();
        workbook.loadFromFile(inputPath);
        //设置文本和字体大小
        Font font = new Font(FOND_ST, Font.PLAIN, 30);
        for (Object object : workbook.getWorksheets()) {
            Worksheet sheet = (Worksheet) object;
            //调用DrawText() 方法插入图片
            BufferedImage imgWtrmrk = drawText(markStr, font, Color.RED, Color.white, sheet.getPageSetup().getPageHeight(), sheet.getPageSetup().getPageWidth());

            //将图片设置为页眉
            sheet.getPageSetup().setLeftHeaderImage(imgWtrmrk);
            sheet.getPageSetup().setLeftHeader("&G");

            //将显示模式设置为Layout
            sheet.setViewMode(ViewMode.Normal);
        }
        //保存文档
        if (inputPath.substring(inputPath.lastIndexOf(".") + 1, inputPath.length()).equalsIgnoreCase("xls")) {
            workbook.saveToFile(outPath, ExcelVersion.Version97to2003);
        } else {
            workbook.saveToFile(outPath, ExcelVersion.Version2010);
        }
    }

    /**
     * excel- 生成水印图片
     * @param text 水印内容
     * @param font 字体
     * @param textColor 水印内容颜色
     * @param backColor 水印背景颜色
     * @param height 图片高度
     * @param width 图片宽度
     * @return
     */
    private static BufferedImage drawText(String text, Font font, Color textColor, Color backColor, double height, double width) {
        //定义图片宽度和高度
        BufferedImage img = new BufferedImage((int) width, (int) height, TYPE_INT_ARGB);
        Graphics2D loGraphic = img.createGraphics();

        //获取文本size
        FontMetrics loFontMetrics = loGraphic.getFontMetrics(font);
        int liStrWidth = loFontMetrics.stringWidth(text);
        int liStrHeight = loFontMetrics.getHeight();

        //文本显示样式及位置
        loGraphic.setColor(backColor);
        loGraphic.fillRect(2, 0, (int) width, (int) height);
        loGraphic.translate(((int) width - liStrWidth) / 2 , ((int) height - liStrHeight) / 2 );
        loGraphic.rotate(Math.toRadians(-25));

        loGraphic.translate(-((int) width - liStrWidth) / 2, -((int) height - liStrHeight) / 2);
        loGraphic.setFont(font);
        loGraphic.setColor(textColor);
        loGraphic.drawString(text, ((int) width - liStrWidth) / 2, ((int) height - liStrHeight) / 2);
        loGraphic.dispose();
        return img;
    }
    //--excel 加水印end-----------------------------------------------------------------------------------------


    //--pdf 加水印start-----------------------------------------------------------------------------------------
    /**
     * PDF 设置水印
     * 免费版仅限于10页的PDF,10页以上的pdf加水印,只加10页水印,并且10页以后的也不会显示
     *
     * @param inFile  原文件路径
     * @param outFile 水印文件路径
     * @param watermark 水印内容
     */
    public static void setPdfWaterMark(String inFile, String outFile, String watermark) {
        //创建PdfDocument对象
        PdfDocument pdf = new PdfDocument();
        //加载示例文档
        pdf.loadFromFile(inFile);

        for (int i = 0; i < pdf.getPages().getCount(); i++) {
            //insertPdfWatermark
            insertPdfWatermark(pdf.getPages().get(i), watermark);
        }
        //保存文档
        pdf.saveToFile(outFile);
        pdf.close();
    }

    /**
     * 水印设置
     * @param page
     * @param watermark 水印内容
     */
    private static void insertPdfWatermark(PdfPageBase page, String watermark) {
        Dimension2D dimension2D = new Dimension();
        dimension2D.setSize(page.getCanvas().getClientSize().getWidth() / 2, page.getCanvas().getClientSize().getHeight() / 3);
        PdfTilingBrush brush = new PdfTilingBrush(dimension2D);
        brush.getGraphics().setTransparency(0.3F);
        brush.getGraphics().save();
        brush.getGraphics().translateTransform((float) brush.getSize().getWidth() / 2, (float) brush.getSize().getHeight() / 2);
        brush.getGraphics().rotateTransform(-45);
        PdfTrueTypeFont font = new PdfTrueTypeFont(new Font(FOND_ST, Font.PLAIN, 12), true);
        brush.getGraphics().drawString(watermark, font, PdfBrushes.getViolet(), 0, 0, new PdfStringFormat(PdfTextAlignment.Center));
        brush.getGraphics().restore();
        brush.getGraphics().setTransparency(1);
        Rectangle2D loRect = new Rectangle2D.Float();
        loRect.setFrame(new Point2D.Float(0, 0), page.getCanvas().getClientSize());
        page.getCanvas().drawRectangle(brush, loRect);
    }
    //--pdf 加水印end-----------------------------------------------------------------------------------------


    //--ppt 加水印start-----------------------------------------------------------------------------------------
    /**
     * PPT设置水印
     * 限制，10页以内可用
     *
     * @param path
     * @param targetpath
     * @param markStr
     * @throws IOException
     */
    public static void setPPTWaterMark(String path, String targetpath, String markStr) throws Exception {
        //创建presentation对象并加载示例文档
        Presentation presentation = new Presentation();
        presentation.loadFromFile(path);
        //限制，10页以内可用，超过10页的有水印
        if (presentation.getSlides().size() > 10) {
            System.out.println("文档大于10页");
        }
        //设置文本水印的宽和高
        int width = 600;
        int height = 300;

        //定义一个长方形区域
        Rectangle2D.Double rect = new Rectangle2D.Double((presentation.getSlideSize().getSize().getWidth() - width) / 2,
                (presentation.getSlideSize().getSize().getHeight() - height) / 2, width, height);

        //添加一个shape到定义区域
        for (int i = 0; i < presentation.getSlides().size(); i++) {
            IAutoShape shape = presentation.getSlides().get(i).getShapes().appendShape(ShapeType.RECTANGLE, rect);

            //设置shape样式
            shape.getFill().setFillType(FillFormatType.NONE);
            shape.getShapeStyle().getLineColor().setColor(Color.white);
            shape.setRotation(-45);
            shape.getLocking().setSelectionProtection(true);
            shape.getLine().setFillType(FillFormatType.NONE);

            //添加文本到shape
            shape.getTextFrame().setText(markStr);
            PortionEx textRange = shape.getTextFrame().getTextRange();

            //设置文本水印样式
            textRange.getFill().setFillType(FillFormatType.SOLID);
            textRange.getFill().getSolidColor().setColor(Color.GRAY);
            textRange.setFontHeight(50);
        }

        //保存文档
        if (path.substring(path.lastIndexOf(".") + 1, path.length()).equalsIgnoreCase("ppt")) {
            presentation.saveToFile(targetpath, com.spire.presentation.FileFormat.PPT);
        } else {
            presentation.saveToFile(targetpath, com.spire.presentation.FileFormat.PPTX_2010);
        }
    }
    //--ppt 加水印end-----------------------------------------------------------------------------------------

}
